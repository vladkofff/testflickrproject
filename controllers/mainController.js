const historyService = require("../services/historyService");
const likesService = require("../services/likesService");
const flickrService = require("../services/flickrService");

const validators = require("../validators");

const getPage = async (req, res, next) => {
    if (!await validators.validateMain(req.query)) {
        res.render("main");
    } else {
        let photos;

        if (req.query.query) {
            try {
                [photos, _] = await Promise.all([
                    flickrService.getByKeyWord(req.query.query, req.query.page ? req.query.page : 1),
                    historyService.addToHistory(req.user._id, req.query.query, req.query.page ? req.query.page : 1)
                ]);
            } catch (e) {
                res.render("main", {sorry: true})
            }

            let likedPhotosIds = (await likesService.getLikesIn(req.user._id, photos.body.photos.photo.map(item => item.id))).map(photo => photo.recordId);

            res.render("main", {
                photos: photos.body.photos.photo,
                likedPhotosIds,
                page: req.query.page ? req.query.page : 1,
                query: req.query.query,
                pageAmount: photos.body.photos.pages
            });
        } else {
            res.render("main");
        }
    }
};
const getHistoryPage = async (req, res, next) => {
    let history = await historyService.getHistory(req.user._id);

    res.render("history", {history})
};
module.exports = {getPage, getHistoryPage};