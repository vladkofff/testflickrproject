const likesService = require("../services/likesService");
const validators = require("../validators");

const like = async (req, res, next) => {
    if(!(await validators.validateLike(req.body))){
        res.redirect("back");
    } else {
        await likesService.likeButtonPressed(req.user._id, req.body.recordId);
        res.redirect("back");
    }
};

module.exports = {like};