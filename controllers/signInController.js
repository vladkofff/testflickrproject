const userService = require("../services/userService");
const validators = require("../validators");

const bcrypt = require("bcrypt");
const passport = require("passport");

const register = async (req, res, next) => {
    if (!await validators.validateRegister(req.body)){
        res.redirect("/sign?error=params");
    }

    try {
        await userService.createUser(req.body.username, await bcrypt.hash(req.body.password, 10));
    } catch (e) {
        res.redirect("/sign?error=register")
    }

    res.redirect("/")
};

const login = passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/sign?error=login',
});

const logout = async (req, res, next) => {
    req.logOut();

    res.redirect("/")
};

const page = async (req, res, next) => {
    res.render("login", {error: req.query.error});
};
module.exports = {register, login, logout, page};