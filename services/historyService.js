const historyModel = require("../db").history;
const userService = require("./userService");

const addToHistory = async (userId, query, page) => {
    const user = await userService.getUserById(userId);

    if (!user) {
        throw new Error("no such user");
    } else {
        return historyModel.insert({userId, query, page: Number(page)})
    }
};
const getHistory = async (userId) => {
    return historyModel.find({userId})
};
module.exports = {addToHistory, getHistory};