const userModel = require("../db").users;

const getUserById = async (id) => {
    return userModel.findOne({_id: id});
};
const getUserByUsername = async (username) => {
    return userModel.findOne({username});
};
const createUser = async (username, password) => {
    const user = await getUserByUsername(username);

    if(user) {
        throw new Error("userExists");
    } else {
        return userModel.insert({username, password});
    }

};
module.exports = {getUserById, getUserByUsername, createUser};