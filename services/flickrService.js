const Flickr = require('flickr-sdk');
const flickr = new Flickr(process.env.FLICKR_KEY);

const config = require("config");

const getByKeyWord = async (query, page) => {
    return flickr.photos.search({text: query, extras: config.get("additionalFlickrParams"), per_page: config.get("per_page"), page, content_type: 1, privacy_filter: 1 });
};

module.exports = {getByKeyWord};