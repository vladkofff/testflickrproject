const likesModel = require("../db").likes;
const userService = require("./userService");

const likeButtonPressed = async (userId, recordId) => {
    const [like, user] = await Promise.all([likesModel.findOne({userId, recordId}), userService.getUserById(userId)]);

    if (!user) {
        throw new Error("no such user")
    }
    else if (like) {
        return likesModel.remove({userId, recordId})
    }
    else {
        return likesModel.insert({userId, recordId});
    }
};
const getLikesIn = async (userId, recordsIdsArray) => {
    return likesModel.find({userId, recordId: {$in: recordsIdsArray}})
};

module.exports = {likeButtonPressed, getLikesIn};