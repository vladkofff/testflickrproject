const joi = require("joi");

const registerSchema = joi.object().keys({
    username: joi.string().alphanum().min(3).max(30).required(),
    password: joi.string().alphanum().min(3).max(30).required()
});
const mainSchema = joi.object().keys({
    page: joi.number().integer().min(1),
    query: joi.string()
});
const likeSchema = joi.object().keys({
    recordId: joi.string().required()
});

const validateRegister = async (object) => {
    return !(await joi.validate(object, registerSchema).error);
};
const validateMain = async (object) => {
    return !(await joi.validate(object, mainSchema).error);
};
const validateLike = async (object) => {
    return !(await joi.validate(object, likeSchema).error);
};
module.exports = {validateRegister, validateMain, validateLike};