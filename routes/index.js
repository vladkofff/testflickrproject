const express = require("express");
const router = express.Router();

const signInRouter = require("./signInRouter");
const mainRouter = require("./mainRouter");
const likesRouter = require("./likesRouter");

const isAuthenticated = require("../middleware/isAuthenticated");

router.use("/sign", signInRouter);
router.use("/likes", isAuthenticated, likesRouter);
router.use("/", isAuthenticated, mainRouter);

module.exports = router;