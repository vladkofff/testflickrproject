const express = require("express");
const router = express.Router();

const mainController = require("../controllers/mainController");

router.get("/", mainController.getPage);
router.get("/history", mainController.getHistoryPage);

module.exports = router;