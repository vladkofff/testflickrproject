const express = require("express");
const router = express.Router();

const signInController = require("../controllers/signInController");

const isAuthenticated = require("../middleware/isAuthenticated");
const isNotAuthenticated = require("../middleware/isNotAuthenticated");

router.get("/", isNotAuthenticated, signInController.page);
router.post("/register", isNotAuthenticated, signInController.register);
router.post("/login", isNotAuthenticated, signInController.login);
router.post("/logout", isAuthenticated, signInController.logout);

module.exports = router;