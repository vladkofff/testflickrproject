const Datastore = require("nedb-promise");
const users = new Datastore({filename: "db/users.db", autoload: true, timestampData: false});
const likes = new Datastore({filename: "db/likes.db", autoload: true, timestampData: false});
const history = new Datastore({filename: "db/history.db", autoload: true, timestampData: false});

module.exports = {users, likes, history};