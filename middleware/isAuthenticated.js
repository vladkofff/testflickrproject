const isAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next()
    }

    res.redirect('/sign')
};

module.exports = isAuthenticated;