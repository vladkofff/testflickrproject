const express = require("express");
const passport = require('passport');
const session = require('express-session');
const helmet = require('helmet');

const userService = require("./services/userService");

const routes = require("./routes");

require("./passport")(passport, userService.getUserByUsername, userService.getUserById);

const app = express();

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(routes);

module.exports = app;

